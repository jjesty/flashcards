package com.jjesty.flashcards.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "learning")
public class Learning {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_word_id")
    private Long userWordId;

    @ManyToOne()
    @JoinColumn(name="learning_status_id")
    private LearningStatus learningStatus;

    @Column(name = "last_repetition")
    private Timestamp lastRepetition;

    @Transient
    private String lastRepetitionString;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserWordId() {
        return userWordId;
    }

    public void setUserWordId(Long userWordId) {
        this.userWordId = userWordId;
    }

    public LearningStatus getLearningStatus() {
        return learningStatus;
    }

    public void setLearningStatus(LearningStatus learningStatus) {
        this.learningStatus = learningStatus;
    }

    public Timestamp getLastRepetition() {
        return lastRepetition;
    }

    public void setLastRepetition(Timestamp lastRepetition) {
        this.lastRepetition = lastRepetition;
    }

    public String getLastRepetitionString() {
        return lastRepetitionString;
    }

    public void setLastRepetitionString(String lastRepetitionString) {
        this.lastRepetitionString = lastRepetitionString;
    }
}