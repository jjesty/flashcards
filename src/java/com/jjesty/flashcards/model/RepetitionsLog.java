package com.jjesty.flashcards.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "repetitions_logs")
public class RepetitionsLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_word_id")
    private Long userWordId;

    @Column(name = "learning_status_id")
    private Long learningStatusId;

    @Column(name = "result")
    private Boolean result;

    @Column(name = "date")
    private Timestamp date;

    @PrePersist
    protected void onCreate() {
        date = new Timestamp(System.currentTimeMillis());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserWordId() {
        return userWordId;
    }

    public void setUserWordId(Long userWordId) {
        this.userWordId = userWordId;
    }

    public Long getLearningStatusId() {
        return learningStatusId;
    }

    public void setLearningStatusId(Long learningStatusId) {
        this.learningStatusId = learningStatusId;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }
}