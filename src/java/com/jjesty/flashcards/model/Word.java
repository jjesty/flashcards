package com.jjesty.flashcards.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "words")
public class Word {
    public Word(String word, String translation, Long createUserId) {
        this.word = word;
        this.translation = translation;
        this.createUserId = createUserId;
    }

    public Word() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "word")
    private String word;

    @Column(name = "translation")
    private String translation;

    @Column(name = "create_user_id")
    private Long createUserId;

    @Column(name = "create_date")
    private Timestamp createDate;

    @PrePersist
    protected void onCreate() {
        createDate = new Timestamp(System.currentTimeMillis());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getTranslation() {
        return translation;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public boolean equal (Word otherWord) {
        return this.translation.equals(otherWord.getTranslation()) && this.word.equals(otherWord.getWord()) && this.createUserId.equals(otherWord.getCreateUserId());
    }
}