package com.jjesty.flashcards.model;

import com.jjesty.flashcards.dao.*;
import com.jjesty.flashcards.service.WordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class WordTraining {

    private Word word;
    private Learning learning;
    private Addiction addiction;
    private UsersWords usersWords;

    @Autowired
    private WordsService wordsService;

    @Autowired
    private WordsDao wordsDao;

    @Autowired
    private LearningDao learningDao;

    @Autowired
    private LearningStatusDao learningStatusDao;

    @Autowired
    private AddictionsDao addictionsDao;

    @Autowired
    private UsersWordsDao usersWordsDao;

    public Word getWord() {
        return word;
    }

    public void setWord(Word word) {
        this.word = word;
    }

    public Learning getLearning() {
        return learning;
    }

    public void setLearning(Learning learning) {
        this.learning = learning;
    }

    public Addiction getAddiction() {
        return addiction;
    }

    public void setAddiction(Addiction addiction) {
        this.addiction = addiction;
    }

    public UsersWords getUsersWords() {
        return usersWords;
    }

    public void setUsersWords(UsersWords usersWords) {
        this.usersWords = usersWords;
    }

    public void saveNewWord () {

        wordsDao.save(word);
        usersWords.setWordId(word.getId());
        usersWordsDao.save(usersWords);
        if(addiction != null && addiction.notEmpty()) {
            addiction.setWordId(word.getId());
            addictionsDao.save(addiction);
        }
    }

    public LearningStatus updateTraining (Boolean result) {
        LearningStatus ls = new LearningStatus();
        if(learning != null) {
            if(result) {
                ls = learningStatusDao.findOne(learning.getLearningStatus().getId() + 1);
            } else {
                ls = learning.getLearningStatus();
            }
        } else {
            //new words should be marked as repeat after day anyway
            List<LearningStatus> learningStatuses = wordsService.getLearningStatuses();
            ls = learningStatuses.get(1);
            learning = new Learning();
            learning.setUserWordId(usersWords.getId());
        }
        learning.setLearningStatus(ls);
        learning.setLastRepetition(new Timestamp(System.currentTimeMillis()));
        learningDao.save(learning);

        return ls;
    }
}
