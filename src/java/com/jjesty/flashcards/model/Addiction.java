package com.jjesty.flashcards.model;

import javax.persistence.*;

@Entity
@Table(name = "addictions")
public class Addiction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "word_id")
    private Long wordId;

    @Column(name = "picture")
    private String picture;

    @Column(name = "transcription")
    private String transcription;

    @Column(name = "example")
    private String example;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getWordId() {
        return wordId;
    }

    public void setWordId(Long wordId) {
        this.wordId = wordId;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTranscription() {
        return transcription;
    }

    public void setTranscription(String transcription) {
        this.transcription = transcription;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }

    public boolean notEmpty() {
        return picture != null || transcription != null || example != null;
    }

    @PrePersist
    protected void onSave() {
        if(picture == null) picture = "";
        if(transcription == null) transcription = "";
        if(example == null) example = "";
    }
}