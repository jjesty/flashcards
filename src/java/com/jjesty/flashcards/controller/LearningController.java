package com.jjesty.flashcards.controller;


import com.jjesty.flashcards.service.UITextesService;
import com.jjesty.flashcards.service.WordsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LearningController {

    private static final Logger logger = LoggerFactory.getLogger(LearningController.class);

    @Autowired
    private UITextesService textesService;

    @Autowired
    private WordsService wordsService;

    @RequestMapping(value = {"/", "/welcome", "/learning"}, method = RequestMethod.GET)
    public String learning(Model model) {
        model.addAttribute("textes", textesService.getTextes());
        Number count = wordsService.getCount();
        if (count.intValue() > 0) {
            model.addAttribute("count", count);
            return "learning";
        } else
            return "begin";
    }




}
