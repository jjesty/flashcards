package com.jjesty.flashcards.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jjesty.flashcards.dao.AddictionsDao;
import com.jjesty.flashcards.dao.RepetitionsLogsDao;
import com.jjesty.flashcards.dao.UsersWordsDao;
import com.jjesty.flashcards.dao.WordsDao;
import com.jjesty.flashcards.model.*;
import com.jjesty.flashcards.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class WordsController {

    private static final Logger logger = LoggerFactory.getLogger(WordsController.class);

    @Autowired
    private UITextesService textesService;

    @Autowired
    private FileWorkService fileWorkService;

    @Autowired
    private WordsService wordsService;

    @Autowired
    private WordTraining wordTraining;

    @Autowired
    private TranslateService translateService;

    @Autowired
    private WordsDao wordsDao;

    @Autowired
    private UsersWordsDao usersWordsDao;

    @Autowired
    private AddictionsDao addictionsDao;

    @Autowired
    private RepetitionsLogsDao repetitionsLogsDao;

    @RequestMapping(value = "/vocabulary", method = RequestMethod.GET)
    public String vocabulary(Model model) {
        model.addAttribute("textes", textesService.getTextes());
        model.addAttribute("words", wordsService.getWordTrainings());

        return "vocabulary";
    }


    @RequestMapping(value = "/words", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Object words(HttpServletRequest request)  {

        return wordsService.getWordTrainings();
    }

    @RequestMapping(value = "/wordbylinkid", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Object word( @RequestParam(value="link", required=true) Integer link)  {
        return wordsService.getWordByLinkId(link);
    }

    @RequestMapping(value = "/statsbylinkid", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Object stats( @RequestParam(value="link", required=true) Integer link)  {
        return wordsService.getStatsByLinkId(link);
    }

    @RequestMapping(value = "/setdeletedbylinkid", method = RequestMethod.POST)
    @ResponseBody
    public void setDeleted(@RequestParam(value="link", required=true) Integer link)  {
        UsersWords usersWords = usersWordsDao.findOne(Long.valueOf(link));
        usersWords.setIsDeleted(true);
        usersWordsDao.save(usersWords);
    }

    @RequestMapping(value = "/learningstatuses", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Object learningStatuses() {
        List <LearningStatus> learningStatusesExtended = new ArrayList<>();
        Map<String,String> descriptions = textesService.getLearningStatuses();
        for (LearningStatus ls : wordsService.getLearningStatuses()) {
            ls.setDescription(descriptions.get(ls.getName()));
            learningStatusesExtended.add(ls);
        }
        return learningStatusesExtended;
    }

    @RequestMapping(value = "/translate", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<String> translate(HttpServletRequest request,
                            @RequestParam(value="word", required=false) String word) throws Exception{
        List<String> translations = wordsService.findTranslations(word);
        translations.addAll(translateService.translate(word));
        return translations.stream().distinct().collect(Collectors.toList());
    }

    @RequestMapping(value = "/learning", method = RequestMethod.POST)
    @ResponseBody
    public void learning(@RequestParam Map<String,String> requestParams) throws Exception{
        String learning=requestParams.get("learning");
        String result=requestParams.get("result");

        WordTraining wt = new ObjectMapper().readValue(learning, WordTraining.class);
        wordTraining.setWord(wt.getWord());
        wordTraining.setLearning(wt.getLearning());
        wordTraining.setAddiction(wt.getAddiction());
        wordTraining.setUsersWords(wt.getUsersWords());

        LearningStatus newLS = wordTraining.updateTraining(Boolean.valueOf(result));

        RepetitionsLog log = new RepetitionsLog();
        log.setResult(Boolean.valueOf(result));
        log.setUserWordId(wt.getUsersWords().getId());
        log.setLearningStatusId(newLS.getId());
        repetitionsLogsDao.save(log);
    }

    @RequestMapping(value = "/training", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Object training(HttpServletRequest request,
                            @RequestParam(value="offset", required=false, defaultValue = "0") Integer offset) {
        return wordsService.getTraining(offset);
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public @ResponseBody
    View upload(HttpServletRequest request,
                  @RequestParam("filename") MultipartFile file) throws Exception {
        if(file.getSize() < 5000000) {
            if (!file.isEmpty()) {
                File uploadDirectory = new File(request.getSession().getServletContext().getRealPath("/resources/tmp"));

                if (!uploadDirectory.exists()) {
                    uploadDirectory.mkdirs();
                }
                String newFilePath = fileWorkService.storeUploaded(file, uploadDirectory).getAbsolutePath();
                fileWorkService.parseFileToDatabase(newFilePath);
            }
        }
        return new RedirectView("vocabulary");
    }

    @RequestMapping(value = "/word", method = RequestMethod.POST)
    public @ResponseBody
    View word(HttpServletRequest request,
              @RequestParam(value="word", required=true) String wordFromRequest,
              @RequestParam(value="translation", required=true) String translationFromRequest,
              @RequestParam(value="wordId", required=false) String wordIdFromRequest,
              @RequestParam(value="addictionId", required=false) String addictionIdFromRequest,
              @RequestParam(value="example", required=false) String exampleFromRequest,
              @RequestParam(value="transcription", required=false) String transcriptionFromRequest,
              @RequestParam(value="picture", required=false) MultipartFile pictureFromRequest) throws Exception {

        Addiction addiction = new Addiction();
        Word word = new Word();

        if(wordIdFromRequest != null && !wordIdFromRequest.equals("")) {
            word = wordsDao.findOne(Long.valueOf(wordIdFromRequest));
            if(addictionIdFromRequest != null && !addictionIdFromRequest.equals("")) {
                addiction = addictionsDao.findOne(Long.valueOf(addictionIdFromRequest));
            }
        }

        word.setWord(wordFromRequest);
        word.setTranslation(translationFromRequest);

        if (exampleFromRequest != null && !exampleFromRequest.equals("")) {
            addiction.setExample(exampleFromRequest);
        }
        if (transcriptionFromRequest != null && !transcriptionFromRequest.equals("")) {
            addiction.setTranscription(transcriptionFromRequest);
        }
        if (!pictureFromRequest.isEmpty()) {
            File uploadDirectory = new File(request.getSession().getServletContext().getRealPath("/resources/uploads"));
            if (!uploadDirectory.exists()) {
                uploadDirectory.mkdirs();
            }
            File img = fileWorkService.compressImage(pictureFromRequest, uploadDirectory);
            addiction.setPicture("/resources/uploads/" + img.getName());
        }

        if(wordIdFromRequest != null && !wordIdFromRequest.equals("")) {
            wordsDao.save(word);
            if(addictionIdFromRequest != null && !addictionIdFromRequest.equals("")) {
                addictionsDao.save(addiction);
            } else {
                addiction.setWordId(Long.valueOf(wordIdFromRequest));
                addictionsDao.save(addiction);
            }
        } else {
            if (addiction.notEmpty()) {
                wordTraining.setAddiction(addiction);
            }
            //add
            word.setCreateUserId(wordsService.findUserId());
            wordTraining.setWord(word);

            UsersWords usersWords = new UsersWords();
            usersWords.setUserId(wordsService.findUserId());
            wordTraining.setUsersWords(usersWords);

            wordTraining.saveNewWord();
        }

        return new RedirectView("vocabulary");
    }



}
