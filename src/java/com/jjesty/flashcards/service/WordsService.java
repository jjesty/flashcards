package com.jjesty.flashcards.service;

import com.jjesty.flashcards.controller.WordsController;
import com.jjesty.flashcards.dao.LearningStatusDao;
import com.jjesty.flashcards.dao.WordsDao;
import com.jjesty.flashcards.model.*;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;


public class WordsService {

    private static final Logger logger = LoggerFactory.getLogger(WordsController.class);

    @Autowired
    private UserServiceImpl userServiceImpl;
    @Autowired
    private SecurityServiceImpl securityServiceImpl;
    @Autowired
    private LearningStatusDao learningStatusDao;
    @Autowired
    private UITextesService textesService;
    @Autowired
    private WordsDao wordsDao;

    @PersistenceContext
    private EntityManager em;

    public List<WordTraining> getWordTrainings() {
        List<WordTraining> trainings = new ArrayList<>();
        try {
            Long user_id = findUserId();
            Query q = em.createNativeQuery(
            "SELECT " +
                "w.id wid, w.word, w.translation, " +
                "ls.id lsid, ls.name, " +
                "l.id lid, l.last_repetition, l.user_word_id, " +
                "a.id aid, a.example, a.picture, a.transcription, " +
                "uw.id uwid, uw.user_id , w.create_user_id crid , w.create_date crdate " +
                "FROM words w\n" +
                "  INNER JOIN users_words uw\n" +
                "    ON w.id = uw.word_id AND NOT uw.is_deleted \n" +
                "  LEFT JOIN learning l\n" +
                "    ON uw.id = l.learning_status_id\n" +
                "  LEFT JOIN learning_statuses ls\n" +
                "    ON l.learning_status_id = ls.id\n" +
                "  LEFT JOIN addictions a\n" +
                "    ON w.id = a.word_id\n" +
                " WHERE uw.user_id = " + user_id  +
                " ORDER BY w.create_date DESC ");
            org.hibernate.Query hibernateQuery = ((org.hibernate.jpa.HibernateQuery) q).getHibernateQuery();
            hibernateQuery.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
            trainings = parseToWordTrainings(q.getResultList());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return trainings;
    }


    public List<String> findTranslations (String w) {
        List<Word> words = wordsDao.findByWord(w);
        List<String> result = new ArrayList<>();
        for (Word word : words) {
            result.add(word.getTranslation());
        }
        return result;
    }

   public Object getWordByLinkId(int linkId) {
        Object res = null;
        try {
            Query q = em.createNativeQuery(
        "SELECT\n" +
                "  w.id word_id, w.word, w.translation, w.create_date, w.create_user_id,\n" +
                "  a.id addiction_id, a.transcription, a.picture, a.example\n" +
                "FROM users_words uw INNER JOIN words w ON uw.word_id = w.id\n" +
                "  LEFT JOIN addictions a ON w.id = a.word_id\n" +
                "WHERE uw.id = " + linkId);
            org.hibernate.Query hibernateQuery = ((org.hibernate.jpa.HibernateQuery) q).getHibernateQuery();
            hibernateQuery.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
            res = q.getSingleResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return res;
    }

   public List getStatsByLinkId(int linkId) {
       List<HashMap> res = null;
        try {
            Query q = em.createNativeQuery(
        "SELECT\n" +
                "  to_char(date, 'DD-MM-YYYY HH:MI:SS') date,\n" +
                "  learning_statuses.name status_name,\n" +
                "  result\n" +
                "FROM repetitions_logs\n" +
                "  LEFT JOIN learning_statuses ON repetitions_logs.learning_status_id = learning_statuses.id\n" +
                "WHERE user_word_id = " + linkId +
                " ORDER BY date DESC");
            org.hibernate.Query hibernateQuery = ((org.hibernate.jpa.HibernateQuery) q).getHibernateQuery();
            hibernateQuery.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
            res = q.getResultList();
            Map<String,String> descriptions = textesService.getLearningStatuses();
            for (HashMap<String,String> r : res) {
                r.put("status_name_description" , descriptions.get(r.get("status_name")));
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return res;
    }

    public List<LearningStatus> getLearningStatuses () {
        List<LearningStatus> learningStatuses = learningStatusDao.findAll();
        learningStatuses.sort(Comparator.comparingLong(LearningStatus::getId));
        return learningStatuses;
    }

    private List<WordTraining> parseToWordTrainings ( List<HashMap> res) {

        List<WordTraining> trainings = new ArrayList<>();

        for (HashMap r : res) {
            WordTraining wt = new WordTraining();
            if(r.get("wid") != null) {
                Word word = new Word();
                word.setId(Long.valueOf((Integer) r.get("wid")));
                word.setWord((String) r.get("word"));
                word.setTranslation((String) r.get("translation"));
                word.setCreateUserId(Long.valueOf((Integer) r.get("crid")));
                word.setCreateDate((Timestamp) r.get("crdate"));
                wt.setWord(word);
            }
            if(r.get("lid") != null) {
                LearningStatus learningStatus = new LearningStatus();
                learningStatus.setId(Long.valueOf((Integer) r.get("lsid")));
                learningStatus.setName((String) r.get("name"));
                learningStatus.setDescription(textesService.getLearningStatusDescriptionByName((String) r.get("name")));
                Learning learning = new Learning();
                learning.setId(Long.valueOf((Integer) r.get("lid")));
                learning.setLastRepetition((Timestamp) r.get("last_repetition"));
                SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");
                learning.setLastRepetitionString(dateFormat.format((Timestamp) r.get("last_repetition")));
                learning.setLearningStatus(learningStatus);
                learning.setUserWordId(Long.valueOf((Integer) r.get("user_word_id")));
                wt.setLearning(learning);
            }

            if(r.get("aid") != null) {
                Addiction addiction = new Addiction();
                addiction.setId(Long.valueOf((Integer) r.get("aid")));
                addiction.setExample((String) r.get("example"));
                addiction.setPicture((String) r.get("picture"));
                addiction.setTranscription((String) r.get("transcription"));
                addiction.setWordId(Long.valueOf((Integer) r.get("wid")));
                wt.setAddiction(addiction);
            }

            if(r.get("uwid") != null) {
                UsersWords usersWords = new UsersWords();
                usersWords.setId(Long.valueOf((Integer) r.get("uwid")));
                usersWords.setUserId(Long.valueOf((Integer) r.get("user_id")));
                usersWords.setWordId(Long.valueOf((Integer) r.get("wid")));
                wt.setUsersWords(usersWords);
            }

            trainings.add(wt);
        }

        return trainings;
    }

    public Long findUserId () {
        return userServiceImpl.findByUsername(securityServiceImpl.findLoggedInUsername()).getId();
    }

    private String onLearningQuery () {
        return "SELECT \n" +
                "   w.id wid, w.word, w.translation, " +
                "   ls.id lsid, ls.name, " +
                "   l.id lid, l.last_repetition, l.user_word_id, " +
                "   a.id aid, a.example, a.picture, a.transcription, " +
                "   uw.id uwid, uw.user_id , w.create_user_id crid, w.create_date crdate   " +
                "FROM words w\n" +
                "INNER JOIN users_words uw ON w.id = uw.word_id AND NOT uw.is_deleted \n" +
                "LEFT JOIN learning l ON uw.id = l.user_word_id\n" +
                "LEFT JOIN learning_statuses ls ON l.learning_status_id = ls.id\n" +
                "LEFT JOIN addictions a ON w.id = a.word_id\n" +
                "WHERE uw.user_id = " + findUserId() + "\n" +
                "      AND (l.id ISNULL\n" +
                "           OR ls.name ~~ 'new' OR ls.name ~~ 'default'\n" +
                "           OR (ls.name !~~ 'done'\n" +
                "               AND l.last_repetition <= current_timestamp - ls.period))\n" +
                "GROUP BY ls.period, ls.id, uw.id, w.id, l.id, a.id\n" +
                "ORDER BY ls.period, ls.id, uw.id, w.id";
    }

    public Object getTraining(int offset) {
        Query q = em.createNativeQuery(onLearningQuery() + " OFFSET " + offset + " LIMIT 1;");
        org.hibernate.Query hibernateQuery = ((org.hibernate.jpa.HibernateQuery) q).getHibernateQuery();
        hibernateQuery.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
        return parseToWordTrainings(q.getResultList());
    }

    public Number getCount() {
        Query q = em.createNativeQuery("SELECT count(*) FROM (" + onLearningQuery() + ") as a;");
        return (Number) q.getSingleResult();
    }

}
