package com.jjesty.flashcards.service;

import com.jjesty.flashcards.controller.WordsController;
import com.jjesty.flashcards.model.*;
import com.opencsv.CSVReader;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FileWorkService {
    private static final Logger logger = LoggerFactory.getLogger(WordsController.class);

    @Autowired
    private WordsService wordsService;

    private List<WordTraining> trainings ;

    @Autowired
    private ApplicationContext appContext;

    public void parseFileToDatabase (String filePath) {

        trainings = wordsService.getWordTrainings();
        try {
            CSVReader reader = new CSVReader(new FileReader(filePath));
            List<String[]> values = reader.readAll();
            for (String[] value: values) {
                String[] line = value[0].split(";");
                Word word = new Word(clean(line[0]), clean(line[1]), wordsService.findUserId());

                if(!existInDB(word)) {
                    WordTraining wt = (WordTraining) appContext.getBean("wordTraining");
                    wt.setWord(word);

                    Addiction addiction = new Addiction();
                    addiction.setPicture(clean(line[2]));
                    addiction.setTranscription(clean(line[3]));
                    addiction.setExample(clean(line[4]));
                    wt.setAddiction(addiction);

                    UsersWords usersWords = new UsersWords();
                    usersWords.setUserId(wordsService.findUserId());
                    wt.setUsersWords(usersWords);

                    wt.saveNewWord();
                }
            }

        } catch (IOException e) {
            logger.error(e.getMessage());
        }

    }

    private boolean existInDB(Word word){
        for (WordTraining training: trainings) {
            if(training.getWord().equal(word)) {
                return true;
            }
        }
        return false;
    }

    public File storeUploaded (MultipartFile file,File dir) throws Exception{
        String name = RandomStringUtils.random(6, true, false)+file.getOriginalFilename();
        if (!file.isEmpty()) {

            byte[] bytes = file.getBytes();

            // Create the file on server
            File serverFile = new File(dir.getAbsolutePath() + File.separator + name);
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
            stream.write(bytes);
            stream.close();

            return serverFile;
        } else {
            throw new FileNotFoundException(file.getOriginalFilename());
        }
    }

    public File compressImage (MultipartFile file, File dir) throws Exception {
        if(!file.isEmpty()) {
            String name = RandomStringUtils.random(6, true, false) + ".jpg";

            InputStream is = file.getInputStream();

            BufferedImage image = ImageIO.read(is);

            // Create the file on server
            File output = new File(dir.getAbsolutePath() + File.separator + name);

            OutputStream out = new FileOutputStream(output);

            ImageWriter writer = ImageIO.getImageWritersByFormatName("jpg").next();
            ImageOutputStream ios = ImageIO.createImageOutputStream(out);
            writer.setOutput(ios);

            //rescale
            int originalWidth = image.getWidth();
            int newWidth = originalWidth;
            int originalHeight = image.getHeight();
            int newHeight = originalHeight;
            int type = image.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : image.getType();

            //rescale to 100 * 100
            if (newHeight > 100) {
                newHeight = 100;
                newWidth = newHeight * originalWidth / originalHeight;
            }
            if (newWidth > 100) {
                newWidth = 100;
                newHeight = newWidth * originalHeight / originalWidth;
            }

            BufferedImage resizedImage = new BufferedImage(newWidth, newHeight, type);
            Graphics2D g = resizedImage.createGraphics();
            g.drawImage(image, 0, 0, newWidth, newHeight, null);
            g.dispose();
            g.setComposite(AlphaComposite.Src);
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            //compress
            ImageWriteParam param = writer.getDefaultWriteParam();
            if (param.canWriteCompressed()) {
                param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
                param.setCompressionQuality(0.9f);
            }

            writer.write(null, new IIOImage(resizedImage, null, null), param);

            out.close();
            ios.close();
            writer.dispose();

            return output;
        } else {
            throw new FileNotFoundException(file.getOriginalFilename());
        }

    }


    private String clean (String str) {
        return str.trim().replace("\"", "");
    }

}
