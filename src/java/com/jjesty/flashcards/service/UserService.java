package com.jjesty.flashcards.service;

import com.jjesty.flashcards.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
