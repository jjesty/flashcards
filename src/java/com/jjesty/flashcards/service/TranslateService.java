package com.jjesty.flashcards.service;

import java.io.*;
import java.net.*;
import java.sql.Timestamp;
import java.util.*;
import javax.net.ssl.HttpsURLConnection;
import com.google.gson.*;
import com.jjesty.flashcards.dao.TranslateCacheDao;
import com.jjesty.flashcards.model.TranslateCache;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class TranslateService {

    private String subscriptionKey;
    private String pathDictionaryLookup;
    private String pathTranslate;
    private String host;
    private String params;
    private String text;

    @Qualifier("translateCacheDao")
    @Autowired
    private TranslateCacheDao translateCacheDao;


    public TranslateService(String subscriptionKey, String host, String pathDictionaryLookup, String pathTranslate, String params) {
        this.subscriptionKey = subscriptionKey;
        this.host = host;
        this.pathDictionaryLookup = pathDictionaryLookup;
        this.pathTranslate = pathTranslate;
        this.params = params;
    }

    public static class RequestBody {
        String Text;

        RequestBody(String text) {
            this.Text = text;
        }
    }

    private String Post (URL url, String content) throws Exception {
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Content-Length", content.length() + "");
        connection.setRequestProperty("Ocp-Apim-Subscription-Key", subscriptionKey);
        connection.setRequestProperty("X-ClientTraceId", java.util.UUID.randomUUID().toString());
        connection.setDoOutput(true);

        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
        byte[] encoded_content = content.getBytes("UTF-8");
        wr.write(encoded_content, 0, encoded_content.length);
        wr.flush();
        wr.close();

        StringBuilder response = new StringBuilder ();
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
        String line;
        while ((line = in.readLine()) != null) {
            response.append(line);
        }
        in.close();

        return response.toString();
    }

    private String loadTranslation () throws Exception {
        String path = pathDictionaryLookup;
        if(text.contains(" ")) {
            path = pathTranslate;
        }
        URL url = new URL(host + path + params);
        List<RequestBody> objList = new ArrayList<RequestBody>();
        objList.add(new RequestBody(text));
        String content = new Gson().toJson(objList);

        return Post(url, content);
    }

    private List <String> parse(String input) {
        return text.contains(" ") ?  parseTranslations(input) :  parseDictionaryLookup(input);
    }

    private List <String> parseTranslations(String input) {

        List <String> translationsList = new ArrayList<>();
        JsonParser parser = new JsonParser();
        JsonObject mainObject = parser.parse(input).getAsJsonArray().get(0).getAsJsonObject();
        JsonArray translations = mainObject.getAsJsonArray("translations");
        for (JsonElement translation : translations) {
            translationsList.add(translation.getAsJsonObject().get("text").getAsString());
        }
        return translationsList;
    }

    private List <String> parseDictionaryLookup(String input) {

        List <String> translationsList = new ArrayList<>();
        JsonParser parser = new JsonParser();
        JsonObject mainObject = parser.parse(input).getAsJsonArray().get(0).getAsJsonObject();
        JsonArray translations = mainObject.getAsJsonArray("translations");
        for (JsonElement translation : translations) {
            translationsList.add(translation.getAsJsonObject().get("displayTarget").getAsString());
        }
        return translationsList;
    }

    private TranslateCache loadFromCache () {
        return translateCacheDao.findByRequest(text);
    }

    private void saveInCache (String response) {
        TranslateCache newRequest = new TranslateCache();
        newRequest.setRequest(text);
        newRequest.setResponse(response);
        translateCacheDao.save(newRequest);
    }

    
    public List <String> translate(String input) throws Exception{
        text = input.trim();
        String response;
        TranslateCache cache = loadFromCache();

        if(cache != null) {
            response = cache.getResponse();
        } else {
            response = loadTranslation();
            saveInCache(response);
        }
        return parse(response);
    }
}
