package com.jjesty.flashcards.service;


import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class UITextesService {
    private Map<String, String> textes = new HashMap<>();
    private Map<String, String> learningStatuses = new HashMap<>();

    public Map<String, String> getTextes() {
        return textes;
    }

    public void setTextes(String asJSON) throws IOException{
        this.textes = new ObjectMapper().readValue(asJSON, HashMap.class);
    }

    public void setLearningStatuses(String asJSON) throws IOException{
        this.learningStatuses = new ObjectMapper().readValue(asJSON, HashMap.class);
    }

    public String getLearningStatusDescriptionByName(String name) {
        return learningStatuses.get(name);
    }

    public Map<String, String> getLearningStatuses() {
        return learningStatuses;
    }
}
