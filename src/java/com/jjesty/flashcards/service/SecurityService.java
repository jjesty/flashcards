package com.jjesty.flashcards.service;

public interface SecurityService {

    String findLoggedInUsername();

    void autoLogin(String username, String password);
}
