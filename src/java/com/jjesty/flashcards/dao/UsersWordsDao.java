package com.jjesty.flashcards.dao;

import com.jjesty.flashcards.model.UsersWords;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersWordsDao extends JpaRepository<UsersWords, Long> {
}
