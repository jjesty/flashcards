package com.jjesty.flashcards.dao;

import com.jjesty.flashcards.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleDao extends JpaRepository<Role, Long> {
}
