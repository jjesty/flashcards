package com.jjesty.flashcards.dao;

import com.jjesty.flashcards.model.Addiction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddictionsDao extends JpaRepository<Addiction, Long> {

}
