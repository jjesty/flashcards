package com.jjesty.flashcards.dao;

import com.jjesty.flashcards.model.TranslateCache;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TranslateCacheDao extends JpaRepository<TranslateCache, Long> {
    TranslateCache findByRequest(String request);
}
