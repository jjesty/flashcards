package com.jjesty.flashcards.dao;

import com.jjesty.flashcards.model.Addiction;
import com.jjesty.flashcards.model.RepetitionsLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepetitionsLogsDao extends JpaRepository<RepetitionsLog, Long> {

}
