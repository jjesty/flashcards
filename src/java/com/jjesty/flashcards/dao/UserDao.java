package com.jjesty.flashcards.dao;

import com.jjesty.flashcards.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
