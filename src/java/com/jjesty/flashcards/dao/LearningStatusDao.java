package com.jjesty.flashcards.dao;

import com.jjesty.flashcards.model.Learning;
import com.jjesty.flashcards.model.LearningStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LearningStatusDao extends JpaRepository<LearningStatus, Long> {

}
