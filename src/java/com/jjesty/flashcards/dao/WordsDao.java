package com.jjesty.flashcards.dao;

import com.jjesty.flashcards.model.Word;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WordsDao extends JpaRepository<Word, Long> {
    List<Word> findByWord(String word);
}
