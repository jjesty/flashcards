package com.jjesty.flashcards.dao;

import com.jjesty.flashcards.model.Learning;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LearningDao extends JpaRepository<Learning, Long> {

}
