--word
CREATE TABLE words (
  id SERIAL NOT NULL PRIMARY KEY,
  word VARCHAR(255) NOT NULL DEFAULT '',
  translation VARCHAR(255) NOT NULL DEFAULT '',
  create_user_id INT NOT NULL DEFAULT 0 REFERENCES users(id),
  create_date TIMESTAMP WITH time zone NOT NULL DEFAULT '1999-01-08 04:05:06 -8:00'
);

--user-words
CREATE TABLE users_words (
  id SERIAL NOT NULL PRIMARY KEY,
  user_id INT NOT NULL DEFAULT 0 REFERENCES users(id),
  word_id INT NOT NULL DEFAULT 0 REFERENCES words(id),
  is_deleted BOOLEAN NOT NULL DEFAULT FALSE,
  UNIQUE (user_id, word_id)
);

--addictions
CREATE TABLE addictions (
  id SERIAL NOT NULL PRIMARY KEY,
  word_id INT NOT NULL DEFAULT 0 REFERENCES words(id),
  picture VARCHAR(255) NOT NULL DEFAULT '',
  transcription VARCHAR(255) NOT NULL DEFAULT '',
  example VARCHAR(255) NOT NULL DEFAULT ''
);

--learning statuses
CREATE TABLE learning_statuses (
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(255) NOT NULL DEFAULT '',
  period INTERVAL NOT NULL DEFAULT '0 days'
);
INSERT INTO learning_statuses VALUES ('new',''),('daily','1 day'),('weekly','1 week'),('monthly','1 month'),('yearly','1 year'),('done','');

--learning
CREATE TABLE learning (
  id SERIAL NOT NULL PRIMARY KEY,
  user_word_id INT NOT NULL DEFAULT 0 REFERENCES users_words(id),
  learning_status_id INT NOT NULL DEFAULT 0 REFERENCES learning_statuses(id),
  last_repetition TIMESTAMP WITH time zone NOT NULL DEFAULT '1999-01-08 04:05:06 -8:00'
);

--repetitions_logs
CREATE TABLE repetitions_logs (
  id SERIAL NOT NULL PRIMARY KEY,
  user_word_id INT NOT NULL DEFAULT 0 REFERENCES users_words(id),
  date TIMESTAMP WITH time zone NOT NULL DEFAULT '1999-01-08 04:05:06 -8:00',
  learning_status_id INT NOT NULL DEFAULT 0 REFERENCES learning_statuses(id),
  result BOOLEAN NOT NULL DEFAULT FALSE
);

