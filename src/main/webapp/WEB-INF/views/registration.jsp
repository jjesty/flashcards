<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=utf-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="${contextPath}/resources/pictures/faviconka_ru_1990.ico">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />


    <title>Создать аккаунт</title>

    <!-- CSS Files -->
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/material-kit.css" rel="stylesheet">

    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/chttp://faviconka.ru/ico/faviconka_ru_1990.icoss" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

</head>

<body class="login-page">
<div class="page-header header-filter" style="background-image: url('${contextPath}/resources/pictures/background.jpg'); background-size: cover; background-position: top center;">

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <div class="card card-signup">
                    <form:form method="POST" modelAttribute="userForm" class="form-signin">
                        <div class="header header-primary text-center">
                            <h2>Регистрация</h2>
                        </div>
                        <div class="card-content">
                            <spring:bind path="username">
                                <div class="input-group ${status.error  ? 'has-error' : ''}">
                                     <span class="input-group-addon"><i class="material-icons">face</i></span>
                                    <form:input type="text" path="username" class="form-control" placeholder="Email"
                                                autofocus="true"></form:input>
                                    <form:errors path="username"></form:errors>
                                </div>
                            </spring:bind>

                            <spring:bind path="password">
                                <div class="input-group ${status.error  ? 'has-error' : ''}">
                                    <span class="input-group-addon"><i class="material-icons">lock_outline</i></span>
                                    <form:input type="password" path="password" class="form-control" placeholder="Password"></form:input>
                                    <form:errors path="password"></form:errors>
                                </div>
                            </spring:bind>

                            <spring:bind path="confirmPassword">
                                <div class="input-group ${status.error  ? 'has-error' : ''}">
                                    <span class="input-group-addon"><i class="material-icons">lock_outline</i></span>
                                    <form:input type="password" path="confirmPassword" class="form-control"
                                                placeholder="Confirm your password"></form:input>
                                    <form:errors path="confirmPassword"></form:errors>
                                </div>
                            </spring:bind>
                        </div>
                        <div class="footer text-center">
                            <button class="btn btn-simple btn-default btn-lg" type="submit">Создать</button>
                            <a class="btn btn-simple btn-default btn-lg" href="${contextPath}/login">Войти в аккаунт</a>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /container -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>