<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=utf-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="${contextPath}/resources/pictures/faviconka_ru_1990.ico">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <title>Тренировка слов</title>

    <!-- CSS Files -->
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/material-kit.css" rel="stylesheet">


    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/chttp://faviconka.ru/ico/faviconka_ru_1990.icoss" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

</head>

<body class="login-page">
<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="${contextPath}/">jj</a>
        </div>

        <div class="collapse navbar-collapse" id="navigation-example">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="${contextPath}/learning">${textes.learning}</a>
                </li>
                <li>
                    <a href="${contextPath}/vocabulary">${textes.dictionary}</a>
                </li>
                <li>
                    <form id="logoutForm" method="POST" action="${contextPath}/logout"><input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/></form>
                    <a class="btn" onclick="document.forms['logoutForm'].submit()">${textes.exit}</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

    <div class="page-header header-filter" style="background-image: url('${contextPath}/resources/pictures/background.jpg'); background-size: cover; background-position: top center;">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                    <div class="card card-signup">
                        <form class="form">
                            <div class="header header-primary text-center">
                                <h2><span id="counter">0</span> ${textes.from} <span id="count">${count}</span></h2>
                            </div>
                            <div class="content">
                                <div id="dateblock" style="display: none">
                                <div class="text-right text-muted">${textes.lastTry}</div>
                                <div class="text-right text-muted"><span id="lastrepetition">----------</span></div>
                                </div>
                                <div class="text-center"><h3><span id="word"></span></h3></div>
                                <div id="transcriptionDiv" class="text-center"><span id="transcription"></span></div>
                                <br>
                                <div id="pictureDiv" class="text-center"><img id="image" class="img-rounded img-raised" src="${contextPath}/resources/pictures/empty.jpg" style="width:100px !important; height:100px !important;" ></div>
                                <br>

                                <div class="progress" >
                                    <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="top" title="" ></div>
                                </div>

                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            </div>
                            <div id="buttonsBlock" class="footer text-center">
                                <div class="row">
                                    <button id="yesbtn" class="btn btn-simple btn-default btn-lg" type="button">${textes.yesKnow}</button>
                                    <button id="nobtn" class="btn btn-simple btn-default btn-lg" type="button">${textes.dontKnow}</button>
                                </div>
                            </div>

                            <div id="translationBlock" class="footer text-center">
                                <div class="row col-md-12">
                                    <div class="text-center"><h3><span id="translation"></span></h3></div>
                                </div>
                                <div class="row">
                                    <button class="btn btn-simple btn-default btn-lg" id="nobtnSuspicious" type="button">${textes.finallyKnow}</button>
                                    <button class="btn btn-simple btn-default btn-lg" id="nextBtn" type="button">${textes.next}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

<!-- /container -->
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/learning.js"></script>
<script src="${contextPath}/resources/js/jquery.loader.min.js"></script>
</body>
</html>