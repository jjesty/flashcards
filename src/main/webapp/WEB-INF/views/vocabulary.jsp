<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=utf-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="${contextPath}/resources/pictures/faviconka_ru_1990.ico">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <title>Словарь</title>

    <!-- CSS Files -->
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/material-kit.css" rel="stylesheet">

    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/chttp://faviconka.ru/ico/faviconka_ru_1990.icoss" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/jquery.loader.min.css">
    <link rel="stylesheet" href="${contextPath}/resources/css/vocabulary.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.16/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/r-2.2.1/datatables.min.css"/>


    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

</head>

<body class="profile-page">
<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="${contextPath}/">jj</a>
        </div>

        <div class="collapse navbar-collapse" id="navigation-example">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="${contextPath}/learning">${textes.learning}</a>
                </li>
                <li>
                    <a href="${contextPath}/vocabulary">${textes.dictionary}</a>
                </li>
                <li>
                    <form id="logoutForm" method="POST" action="${contextPath}/logout"><input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/></form>
                    <a class="btn" onclick="document.forms['logoutForm'].submit()">${textes.exit}</a>
                </li>
            </ul>
        </div>
    </div>
</nav>


    <div class="page-header header-filter" data-parallax="true" style="background-image: url('${contextPath}/resources/pictures/background.jpg');">
        <div class="container">
            <div class="col-md-6">
                <h1 class="title">${textes.dictionary}</h1>
                <h4>${textes.dictionaryDescription}</h4>
            </div>
        </div>
    </div>

    <div class="main main-raised">
        <div class="container">
                <div class="row">
                    <div class="col-md-12 top-block">
                        <div class="input-group card card-raised">
                            <div class="card-content">
                                <div class="input-group" id="topBlock">
                                    <c:if test="${!empty words}">
                                    <input id="searchField" type="search" class="form-control" placeholder="Поиск">
                                    <div class="input-group-addon" ><a><i class="material-icons">find_in_page</i></a></div>
                                    </c:if>
                                    <div class="input-group-addon" ><a id="addWordBtn" href="#" data-toggle="modal" data-target="#modalWordDiv">${textes.addWord}</a></div>
                                    <div class="input-group-addon" ><a href="#" data-toggle="modal" data-target="#addWordsFromFileDiv">${textes.loadFromFile}</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <c:if test="${!empty words}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">

                             <table class="table" id="vocabularyTable" style="display: none">
                                <thead>
                                    <tr>
                                        <th class="text-center">${textes.word}</th>
                                        <th class="text-center">${textes.translation}</th>
                                        <th class="text-right"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${words}" var="word">
                                    <tr>
                                        <td>${word.word.word}</td>
                                        <td>${word.word.translation}</td>
                                        <td class="td-actions text-right" data-link_id="${word.usersWords.id}">
                                            <button type="button" rel="tooltip" title="" class="btn btn-info btn-simple btn-xs" name="infoBtn">
                                                <i class="fa fa-book"></i>
                                            </button>
                                            <button type="button" rel="tooltip" title="" class="btn btn-success btn-simple btn-xs" name="editBtn">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            <button type="button" rel="tooltip" title="" class="btn btn-danger btn-simple btn-xs" name="delBtn">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </c:forEach>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                </c:if>
        </div>
    </div>
</div>
<%--hidden forms--%>
<div class="modal fade" id="modalWordDiv" tabindex="-1" role="dialog" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" class="form-inline" id="modalWordForm" action="${contextPath}/word?${_csrf.parameterName}=${_csrf.token}" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="material-icons">clear</i>
                    </button>
                    <h4 class="modal-title">Добавить слово</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="wordId" id="wordId" >
                        <input type="hidden" name="addictionId" id="addictionId" >
                        <div class="col-md-5">
                            <label class="col-md-6 text-primary" for="word">${textes.word}: </label>
                            <div class="input-group"  id="inputGroupWord">
                                <input type="text" class="form-control" name="word" id="word" placeholder="Слово для изучения">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="col-md-6 text-primary" for="translation">${textes.translation}: </label>
                            <div class="input-group ">
                                <div class="input-group-addon" id="loadTranslation"><a href="#" onclick="loadTranslation();" title="${textes.loadTranslation}"><i class="material-icons">lightbulb_outline</i></a></div>
                                <input type="text" class="form-control" name="translation" id="translation" placeholder="${textes.translation}">
                            </div>
                        </div>
                        <div class="col-md-1 collapse-arrow">
                            <a data-toggle="collapse" href="#extendedInfo" aria-expanded="false" aria-controls="extendedInfo"><i class="material-icons">keyboard_arrow_down</i></a>
                        </div>
                    </div>
                    <div class="row" style="display: none;" id="translationError">
                        <div class="alert alert-primary" role="alert">
                            ${textes.needTranslate}
                        </div>
                    </div><div class="row" style="display: none;" id="wordError">
                        <div class="alert alert-primary" role="alert">
                            ${textes.needWord}
                        </div>
                    </div>
                    <div class="row" style="display: none;" id="wordLoadingError">
                        <div class="alert alert-primary" role="alert">
                            ${textes.translationError}
                        </div>
                    </div>
                    <div class="collapse" id="extendedInfo">
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <label class="col-md-12 text-primary" for="picture">  ${textes.picture} :</label>
                                <div class="col-md-12">
                                    <div class="text-center">
                                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail img-rounded img-raised">
                                                <img id="imgPreview" src="${contextPath}/resources/pictures/empty.jpg" width="100" height="100" alt="...">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail img-rounded img-raised"></div>
                                            <div>
                                                <span class="btn btn-raised btn-round btn-default btn-file btn-sm">
                                                    <span class="fileinput-new"> ${textes.load}</span>
                                                <span class="fileinput-exists"> ${textes.edit}</span>
                                                <input type="file" id="picture" name="picture" />
                                                </span>
                                                <a class="btn btn-danger btn-round fileinput-exists  btn-sm" data-dismiss="fileinput"><i class="fa fa-times"></i> ${textes.del} </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <label class="col-md-12 text-primary" for="transcription"> ${textes.transcription}</label>
                                <div class="input-group col-md-12">
                                    <input type="text" class="form-control" id="transcription" name="transcription" placeholder="transcription">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <label class="col-md-12 text-primary" for="example"> ${textes.example} </label>
                                <div class="input-group col-md-12">
                                    <input type="text" class="form-control" id="example" name="example" placeholder="example">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default btn-simple">ОК</button>
                        <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">${textes.cancel}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="addWordsFromFileDiv" tabindex="-1" role="dialog" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" id="loadFromFileForm" class="form-inline" action="${contextPath}/upload?${_csrf.parameterName}=${_csrf.token}" enctype="multipart/form-data" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="material-icons">clear</i>
                    </button>
                    <h4 class="modal-title">Импорт из файла</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="form-group">
                                ${textes.fileCondition}
                            </div>
                            <div class="form-group">
                                <label for="inputFile">${textes.inputFile}</label>
                                <input id="inputFile" name="filename" type="file">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default btn-simple">ОК</button>
                    <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">${textes.cancel}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="loadStatsDiv" tabindex="-1" role="dialog" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-inline" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="material-icons">clear</i>
                    </button>
                    <h4 class="modal-title">${textes.stats}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="col-md-12 ">
                                <div class="text-center">
                                    <table class="table" id="statsTable">
                                        <thead>
                                        <tr>
                                            <th class="text-center">${textes.date}</th>
                                            <th class="text-center">${textes.status}</th>
                                            <th class="text-center">${textes.result}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="btn btn-default btn-simple">ОК</button>
                </div>
            </form>
        </div>
    </div>
</div>
<%----%>
<!-- /container -->
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${contextPath}/resources/js/vocabulary.js"></script>
<script src="${contextPath}/resources/js/jquery.loader.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.16/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/r-2.2.1/datatables.min.js"></script>

</body>
</html>