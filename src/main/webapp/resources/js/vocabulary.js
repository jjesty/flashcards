$(function () {
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function(e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });
});

function showLoader() {
    $.loader({
        className:"blue-with-image-2",
        content:''
    });
}
function hideLoader() {
    $.loader("close");
}

function fieldValid(field, lang) {
    var maxLength = 40;
    var expressions = {"eng": /^[a-zA-Z\s]*$/, "rus" : /^[\u0400-\u04FF\s]*$/};
    return typeof field === "string" && field.length < maxLength && field !== "" && expressions[lang].test(field.toLowerCase());
}

function loadTranslation() {
    if (fieldValid($("#word").val(), "eng")) {
        showLoader();
        $.get("/translate", {word: $("#word").val()}, function (data) {
            if(data.length > 0) {
                var newElem = "<select class=\"selectpicker \" name=\"translation\" data-style=\"select-with-transition\" id=\"translation\">";
                $.each(data, function (key, value) {
                    newElem += "<option>" + value + "</option>";
                });
                newElem += "</select>";
                $("#translation").replaceWith(newElem);
                $("#loadTranslation").hide();
                $('.selectpicker').selectpicker({style: 'select-with-transition'});

                //for fix bad styles
                $("#translation").parent().removeClass("input-group-btn").addClass("myfix");

                hideErrors();
            } else {
                $("#wordLoadingError").fadeIn("slow");
            }
            hideLoader();
        });
    } else {
        $("#wordError").fadeIn("slow");
        $("#inputGroupWord").addClass("has-error")
    }
}

$("#word").change(function () {
    $("#loadTranslation").show();
    $(".selectpicker#translation").parent("div").replaceWith("<input type=\"text\" class=\"form-control\" name=\"translation\" id=\"translation\" placeholder=\"Перевод\">");
    $("#translation").change(function () {hideErrors()});
    hideErrors();
});



$("#loadFromFileForm").on('submit', function(e){
    showLoader();
});
$("#modalWordForm").submit(function(event){
    showLoader();
    var errors = wordFormErrors();
    if(errors.length > 0 ){
        event.preventDefault();
        $.each(errors, function (key, error) { $("#"+error+"Error").show();});
    }
    hideLoader();
});

function wordFormErrors() {
    var errors = {
        "word" : !fieldValid($("#word").val(), "eng"),
        "translation" : !fieldValid($("#translation").val(), "rus")
    };
    return Object.keys(errors)
        .filter(function(key) {return errors[key]});
}

function hideErrors() {
    $("#wordError").fadeOut("slow");
    $("#translationError").fadeOut("slow");
    $("#wordLoadingError").fadeOut("slow");
    $("#inputGroupWord").removeClass("has-error")
}

$( document ).ready(function() {
    $('#vocabularyTable').DataTable({
        "dom": "<'row'<'col-sm-4'i><'col-sm-3'l>f<'col-sm-5'p>>" + "<'row'<'col-sm-12'tr>>",
        "initComplete": function(){
            $("#vocabularyTable_filter").find("input").detach().prependTo('#topBlock');
            $("#vocabularyTable_filter").remove();
            $("#searchField").remove();
            $("#vocabularyTable").show();
        },
        "language": {
        }
    });
});

$("[name=editBtn]").click(function () {
    clearModalWord();
    var link = $(this).parent().data("link_id");
    if(link > 0) {
        showLoader();
        $.get("/wordbylinkid", {link: link}, function (data) {
            if (!$.isEmptyObject(data)) {
                if (typeof data["word_id"] !== "undefined") {
                    var imgSrc = "/resources/pictures/empty.jpg";
                    $("#modalWordForm").data("data", data);
                    $("#wordId").val(data["word_id"]);
                    $("#word").val(data["word"]);
                    $("#translation").val(data["translation"]);
                    if (typeof data["addiction_id"] !== "undefined" && data["addiction_id"]) {
                        $("#addictionId").val(data["addiction_id"]);
                        $("#transcription").val(data["transcription"]);
                        $("#example").val(data["example"]);
                        if(data["picture"] && data["picture"] !== "") {
                            imgSrc = data["picture"];
                        }
                    }
                    $("#imgPreview").attr("src", imgSrc);
                    $("#modalWordDiv").modal();
                }
            }
            hideLoader();
        });
    }
});

$("[name=delBtn]").click(function () {
    var link = $(this).parent().data("link_id");
    var tr = $(this).parents('tr');
    if(link > 0) {
        showLoader();
        $.post("/setdeletedbylinkid", {link: link}, function () {
            var table = $("#vocabularyTable").DataTable();
            table.row(tr).remove().draw();
            hideLoader();
        });
    }
});
$("[name=infoBtn]").click(function () {
    var link = $(this).parent().data("link_id");
    if(link > 0) {
        showLoader();
        var tbody = $("#statsTable").find("tbody");
        tbody.find("tr").remove();
        $.get("/statsbylinkid", {link: link}, function (data) {
            if (!$.isEmptyObject(data)) {
                $.each(data, function (key, value) {
                    tbody.append("<tr>"+
                        "<td>"+value["date"]+"</td>" +
                        "<td>"+value["status_name_description"]+"</td>" +
                        "<td><input type=\"checkbox\" disabled " + (value["result"] ? " checked " : "") + " /></td>" +
                        "</tr>");
                });
            }
            $("#loadStatsDiv").modal();
            hideLoader();
        });
    }
});

$("#addWordBtn").click(function (e) {
    clearModalWord();
   $("#word").val($("#topBlock").find("input[type=search]").val());
});

function clearModalWord () {
    $("#wordId").val("");
    $("#addictionId").val("");
    $("#word").val("");
    $("#translation").val("");
    $("#transcription").val("");
    $("#example").val("");
    $("#imgPreview").attr("src", "/resources/pictures/empty.jpg");
}
