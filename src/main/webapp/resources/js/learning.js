$(function () {
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function(e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });
});

function showLoader() {
    $.loader({
        className:"blue-with-image-2",
        content:''
    });
}
function hideLoader() {
    $.loader("close");
}

function loadWord() {
    showLoader();
    $("#translationBlock").hide();
    $("#buttonsBlock").fadeIn('slow');
    if (parseInt($("#counter").text()) < parseInt($("#count").text())) {
        $.get("/training", function (training) {
            if(training.length > 0) {
                setTraining(training[0]);
                hideLoader();
            } else location.reload();
        });
    } else location.reload();
}

function setTraining (training) {
    $("#translationBlock").data("training", training);

    $("#word").html(training["word"]["word"]);
    $("#translation").html(training["word"]["translation"]);

    $("#counter").html(parseInt($("#counter").text())+1);

    setAddiction(training);
    setProgressBar(training);
    setLastRepetition(training);

}

function setAddiction(training) {
    picturePath = "/resources/pictures/empty.jpg";
    transcription = "";
    if(typeof training["addiction"] !== "undefined" && training["addiction"]) {
        if (typeof training["addiction"]["transcription"] !== "undefined" && training["addiction"]["transcription"]) {
            transcription = "[" + training["addiction"]["transcription"] + "]";
        }
        if (typeof training["addiction"]["picture"] !== "undefined" && training["addiction"]["picture"]) {
            picturePath = training["addiction"]["picture"];
        }
    }
    $("#transcription").html(transcription);
    $("#image").attr("src", picturePath).height(100).width(100);
}

function setLastRepetition(training) {
    if(typeof training["learning"] !== "undefined" && training["learning"]) {
        if (typeof training["learning"]["lastRepetitionString"] !== "undefined" && training["learning"]["lastRepetitionString"]) {
            $("#lastrepetition").html(training["learning"]["lastRepetitionString"]);
            $("#dateblock").fadeIn('slow');
        } else $("#dateblock").hide();
    } else $("#dateblock").hide();
}

function setProgressBar(training) {
    //set progress
    var bar = $(".progress-bar");
    var ind = 1;
    var desc = bar.data("learningstatuses")[0]["description"];
    if(typeof training["learning"] !== "undefined" && training["learning"]) {
        if (typeof training["learning"]["learningStatus"] !== "undefined" && training["learning"]["learningStatus"]) {
            $.each(bar.data("learningstatuses"), function (key, value) {
                if (value["id"] === training["learning"]["learningStatus"]["id"]) {
                    ind = key;
                }
            });
            desc = training["learning"]["learningStatus"]["description"];
        }
    }

    bar.attr("title", desc).tooltip('fixTitle').tooltip();
    bar.attr('aria-valuenow', (ind + 1) * bar.data("step"));
    bar.css('width',(ind + 1) * bar.data("step") + "%" );
    $('[data-toggle="tooltip"]').tooltip();
}

$("#yesbtn").click(function () {
    onclickButtonsAtFirst(true);
});
$("#nobtn").click(function () {
    onclickButtonsAtFirst(false);
});
$("#nobtnSuspicious").click(function () {
    onclickBtnAtSecond(false);
});
$("#nextBtn").click(function () {
    onclickBtnAtSecond(true);
});

function onclickButtonsAtFirst(isPositive) {
    $("#translationBlock").data("isPositive", isPositive);

    $(".progress-bar").tooltip("hide");
    $("#translationBlock").fadeIn('slow');
    $("#buttonsBlock").hide();
    if(isPositive) $("#nobtnSuspicious").show();
    else  $("#nobtnSuspicious").hide();
}

function onclickBtnAtSecond(isPositive) {
    var isPositiveFinally = isPositive && $("#translationBlock").data("isPositive");
    $("#translationBlock").data("userWordId");
    $.post("/learning", { learning: JSON.stringify($("#translationBlock").data("training")), result: isPositiveFinally }, function() {
       loadWord();
    });
}

$( document ).ready(function() {
    load();
});

function load() {
    $.get("/learningstatuses", function(learningstatuses) {
        $(".progress-bar")
            .data("step", 100/learningstatuses.length)
            .data("learningstatuses", learningstatuses.sort(function (a,b) {return a.id > b.id;}));
        loadWord();
    });
}

